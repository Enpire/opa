﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wheel : MonoBehaviour
{

	public static Wheel Instance;

	public float SpeedMin;
	public float SpeedMax;
	public float TimeToGrowSpeed;
	public bool Direction;
	public float CurrentSpeed;

	private Transform _t;

	float Speed()
	{
		var time = Time.time - Game.Instance.StartTime;
		if (time > TimeToGrowSpeed) time = TimeToGrowSpeed;
		return (SpeedMin + (SpeedMax - SpeedMin) * time / TimeToGrowSpeed) * (Direction ? 1 : -1);
	}

	void Start()
	{
		Instance = this;
		_t = transform;
	}
	
	void Update () {
		if (Game.Instance.state == Game.State.Play)
		{
			CurrentSpeed = Speed();
			var step = CurrentSpeed * Time.deltaTime * 50;
			_t.localRotation = Quaternion.Euler(0, 0, step) * _t.localRotation;
		}	
	}

	public void ToggleDirection()
	{
		Direction = !Direction;
	}
}
