﻿using UnityEngine;
using UnityEngine.UI;

public class RestoreButtonBlocker : MonoBehaviour
{

	public Button Button;
	
	private void OnEnable()
	{
		Button.interactable = Game.Instance.restoreCounter < 1;
	}
}
