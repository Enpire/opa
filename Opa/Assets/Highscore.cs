﻿using UnityEngine;
using UnityEngine.UI;

public class Highscore : MonoBehaviour
{

    public Text Text;
    public bool Continious;
    public string Template = "Highscore: {0}";

    private void OnEnable()
    {
        if (Game.Instance != null)
            Text.text = string.Format(Template, Game.Instance.Highscrore);
    }

    private void Start()
    {
        Text.text = string.Format(Template, Game.Instance.Highscrore);
    }

    // Update is called once per frame
    void Update ()
    {
        if (!Continious) return;
        Text.text = string.Format(Template, Game.Instance.Highscrore);
    }
}