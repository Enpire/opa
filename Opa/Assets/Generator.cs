﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class Generator : MonoBehaviour
{

	public float LastGenerated;
	public float DelayMin;
	public float DelayMax;
	public float Delay;
	public float MinAngleDistance;
	public float Timeout = 2F; 

	public GameObject Good;
	public GameObject Danger;
	public int DangerChance;
	
	// Update is called once per frame
	void Update () {
		if (Game.Instance.state == Game.State.Play && Game.Instance.StartTime + Timeout < Time.time)
		{
			if (Time.time >= Delay + LastGenerated) Generate();
		}
		else UpdateDelay();
	}

	void UpdateDelay()
	{
		Delay = DelayMin + (DelayMax - DelayMin) * Random.value;
	}

	void Generate()
	{
		UpdateDelay();
		LastGenerated = Time.time;
		bool isDanger = (int) (Random.value * 100) < DangerChance;
		var item = isDanger ? Danger : Good;
		var rotation = MinAngleDistance + (360 - 2 * MinAngleDistance) * Random.value;
		var z = Wheel.Instance.transform.localEulerAngles.z % 360;
		Instantiate(item, Vector3.zero, Quaternion.Euler(0, 0, z + rotation));
	}
}
