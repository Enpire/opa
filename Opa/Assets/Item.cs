﻿using System;
using UnityEngine;

public class Item : MonoBehaviour {

	public enum Type
	{
		Good,
		Danger,
	}

	public Type type;
	public int Score;
	public float BornTime;
	public float Timeout;
	public float scaleMax = 8F;
	public float scaleMin = 1F;
	public float minProgress = 0.1F;
	
	private void OnTriggerEnter2D(Collider2D other)
	{
		if (!other.CompareTag("Handle")) return;
		switch (type)
		{
			case Type.Good:
				Game.Instance.AddScore((int) (Score * Progress()));
				break;
			case Type.Danger:
				Game.Instance.Damage((int) (Score * Progress()));
				break;
			default:
				throw new ArgumentOutOfRangeException();
		}
		Remove();
	}

	private void Start()
	{
		BornTime = Time.time;
	}

	private float Progress()
	{
		var progress = (Time.time - BornTime) / Timeout;
		if (type == Type.Good) progress = 1 - progress;
		return Math.Max(progress, minProgress);
	}

	private float Scale()
	{
		var scale = scaleMin + (scaleMax - scaleMin) * Progress();
		return scale;
	}

	private void Update()
	{
		if (Timeout <= 0) return;
		if (BornTime + Timeout > Time.time)
		{
			transform.localScale = Vector3.one * Scale();
			return;
		}
		Remove();
	}

	public void Remove()
	{
		Destroy(transform.parent.gameObject);
	}
}
