﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HpWatcher : MonoBehaviour
{

	public Game game;
	public Image Image;
	
	// Use this for initialization
	void Start ()
	{
		game = FindObjectOfType<Game>();
		Image = GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
		if (Image != null && game != null)
		{
			Image.fillAmount = game.hp / 100F;
		}
	}
}
