﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpeedBar : MonoBehaviour
{

	public Wheel Wheel;
	public Image Bar;
	public Text Current;
	public InputField Min;
	public InputField Max;
	public InputField Time;
	

	private void Start()
	{
		Min.text = PlayerPrefs.GetFloat("min", Wheel.SpeedMin).ToString();
		Max.text = PlayerPrefs.GetFloat("max", Wheel.SpeedMax).ToString();
		Time.text = PlayerPrefs.GetFloat("time", Wheel.TimeToGrowSpeed).ToString();
	}

	// Update is called once per frame
	void Update ()
	{
		var speed = Math.Floor(Math.Abs(Wheel.CurrentSpeed) * 100) / 100;
		Current.text = speed.ToString();
		var progress = (speed - Wheel.SpeedMin) / (Wheel.SpeedMax - Wheel.SpeedMin);
		Bar.fillAmount = (float) progress;
	}

	public void OnMinChanged(string _value)
	{
		var value = float.Parse(_value);
		Wheel.SpeedMin = value;
		PlayerPrefs.SetFloat("min", value);
	}

	public void OnMaxChanged(string _value)
	{
		var value = float.Parse(_value);
		Wheel.SpeedMax = value;
		PlayerPrefs.SetFloat("max", value);
	}

	public void OnTimeChanged(string _value)
	{
		var value = float.Parse(_value);
		Wheel.TimeToGrowSpeed = value;
		PlayerPrefs.SetFloat("time", value);
	}
}
