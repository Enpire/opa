﻿using System;
using UnityEngine;

public class Game : MonoBehaviour {

	public enum State
	{
		Start,
		Play,
		Pause,
		Gameover,
	}

	public static Game Instance;
	public float StartTime;
	public int Score;
	public int Highscrore;
	public int hp = 100;
	public int restoreCounter = 0;

	public State state;

	// Use this for initialization
	private void OnEnable()
	{
		Instance = this;
	}

	private void Start()
	{
		Highscrore = PlayerPrefs.GetInt("highscrore", 0);
	}

	public void Play()
	{
		if (state != State.Gameover && state != State.Start) return;
		state = State.Play;
		StartTime = Time.time;
		Score = 0;
		hp = 100;
		Time.timeScale = 1F;
		restoreCounter = 0;
	}

	public void Restore()
	{
		if (state != State.Gameover) return;
		state = State.Play;
		hp = 100;
		Time.timeScale = 1F;
		restoreCounter += 1;
	}

	public void AddScore(int value = 1)
	{
		Score += value;
		if (Score <= Highscrore) return;
		Highscrore = Score;
		PlayerPrefs.SetInt("highscrore", Highscrore);
	}

	public void Damage(int value)
	{
		print("Damage: " + value);
		hp = Math.Max(hp - value, 0);
		if (hp == 0)
		{
			GameOver();
		}
	}

	public void GameOver()
	{
		if (state == State.Play) state = State.Gameover;
		Time.timeScale = 0;
		foreach (var item in FindObjectsOfType<Item>())
		{
			item.Remove();
		}
	}
}
