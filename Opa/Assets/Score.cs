﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{

	public Text Text;
	public bool Continious;
	public string Template = "{0}";

	private void OnEnable()
	{
		if (Game.Instance != null)
			Text.text = string.Format(Template, Game.Instance.Score);
	}

	private void Start()
	{
		Text.text = string.Format(Template, Game.Instance.Score);
	}

	// Update is called once per frame
	void Update ()
	{
		if (!Continious) return;
		Text.text = string.Format(Template, Game.Instance.Score);
	}
}
