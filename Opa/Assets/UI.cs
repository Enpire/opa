﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI : MonoBehaviour
{

	public GameObject StartScreen;
	public GameObject GameOverScreen;

	void Update () {
		StartScreen.SetActive(Game.Instance.state == Game.State.Start);
		GameOverScreen.SetActive(Game.Instance.state == Game.State.Gameover);
	}
}
